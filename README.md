# Pagination Example N.3 Frontend

An advanced pagination example using React. Run the following commands:

```bash
npm run transpilation
npm start
```

After that, point your browser to the URLs listed in your CLI.

> Don't forget to deploy the back-end server!
