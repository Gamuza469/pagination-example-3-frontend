class CommentTable extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            selectedPage: 1,
            newPage: 1,
            elementsPerPage: 15,
            comments: []
        };

        this.handlePageChange = this.handlePageChange.bind(this);
        this.handleElementsChange = this.handleElementsChange.bind(this);
    }

    componentDidMount(){
        const {selectedPage, elementsPerPage} = this.state;
        fetch(`http://localhost:8000?pageToGet=${selectedPage}&commentsPerPage=${elementsPerPage}`)
            .then(response => response.json())
            .then(data => this.setState({ ...data }));
    }

    handlePageChange(event) {
        this.setState({
            newPage: event.target.value
        });
    }

    handleElementsChange(event) {
        this.setState({
            elementsPerPage: event.target.value
        });
    }

    getPage () {
        const {newPage, elementsPerPage} = this.state;
        fetch(`http://localhost:8000?pageToGet=${newPage}&commentsPerPage=${elementsPerPage}`)
            .then(response => response.json())
            .then(data => this.setState({ ...data }));
    }

    render() {
        const { comments, count, pages, elementsPerPage, selectedPage } = this.state;

        return (
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>PostId</th>
                            <th>Name</th>
                            <th>E-Mail</th>
                            <th>Comment</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            comments.map(comment => {
                                const { id, postId, name, email, body } = comment;

                                return (
                                    <tr key={id}>
                                        <td>{id}</td>
                                        <td>{postId}</td>
                                        <td>{name}</td>
                                        <td>{email}</td>
                                        <td>{body}</td>
                                    </tr>
                                );
                            })
                        }
                    </tbody>
                </table>

                <div>
                    Current page: {selectedPage} <br />
                    Comment count: {count} <br />
                    Available pages: {pages} <br />
                    Elements per page: {elementsPerPage} <br />
                </div>

                <label>Page to get</label>
                <input type="number" name="pageToGet" value={this.state.newPage} onChange={this.handlePageChange} />
                <label>Elements per page</label>
                <input type="number" name="elementsPerPage" value={this.state.elementsPerPage} onChange={this.handleElementsChange} />

                <button onClick={() => this.getPage()}>Get page</button>
            </div>
        );
    }
}

export default CommentTable;
