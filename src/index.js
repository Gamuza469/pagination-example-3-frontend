import CommentTable from './components/CommentTable.js';

ReactDOM.render(
    <CommentTable />,
    document.getElementById('root')
);
